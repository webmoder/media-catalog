<?php
/**
 * Created by PhpStorm.
 * User: web-m
 * Date: 22.03.2016
 * Time: 16:29
 */

require __DIR__.'/vendor/autoload.php';

spl_autoload_register(function ($class) {
    require_once __DIR__ . '/' .$class . '.php';
});

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$IlluminateConfigs = require_once __DIR__.'/configs/datebase.php';

$capsule->addConnection($IlluminateConfigs['mysql']);

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();

$loader = new Twig_Loader_Filesystem(__DIR__.'/MediaCatalog/Views');
$twig = new Twig_Environment($loader, array(
    'cache' => __DIR__.'/MediaCatalog/Cache/Views',
));

MediaCatalog\Views\View::setTwig($twig);

require_once __DIR__ . '/MediaCatalog/routes.php';