<?php
/**
 * Created by PhpStorm.
 * User: web-m
 * Date: 22.03.2016
 * Time: 17:18
 */
require_once __DIR__ . '/../bootstrap.php';

$route = $_SERVER['REQUEST_URI'];
$method = strtolower($_SERVER['REQUEST_METHOD']);

echo \MediaCatalog\Http\Route::execute($route, $method);