<?php
/**
 * Created by PhpStorm.
 * User: web-m
 * Date: 22.03.2016
 * Time: 12:33
 */

namespace MediaCatalog\Controllers;

use MediaCatalog\Models\Tag;
use MediaCatalog\Views\View;

class TagsController extends  Controller
{
    //rest actions
    public function index(){
        $tags = Tag::all();

        return $tags;
    }
    public function show($tag_id){
        $tag = Tag::with('posts')->findOrFail($tag_id);

        return View::render('Tags/single.twig', ['tag' => $tag]);
    }
}