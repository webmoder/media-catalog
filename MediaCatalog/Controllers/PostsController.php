<?php
/**
 * Created by PhpStorm.
 * User: web-m
 * Date: 22.03.2016
 * Time: 12:25
 */

namespace MediaCatalog\Controllers;

use MediaCatalog\Models\Post;
use MediaCatalog\Views\View;

class PostsController extends Controller
{
    //rest actions
    public function index(){
        $posts = Post::with('tags')->get();

        return View::render('Posts/list.twig',['posts' => $posts]);
    }
    public function show($post_id){
        $post = Post::with('tags')->findOrFail($post_id);

        return View::render('Posts/single.twig', ['post' => $post]);
    }
}