<?php
/**
 * Created by PhpStorm.
 * User: web-m
 * Date: 22.03.2016
 * Time: 12:45
 */

namespace MediaCatalog\Views;


class View
{
    /**
     * @var \Twig_Environment $twig
     */
    static protected $twig;

    static public function setTwig($twig){
        self::$twig = $twig;
    }

    static public function render($template, $data = []){
        return self::$twig->render($template, $data);
    }
}