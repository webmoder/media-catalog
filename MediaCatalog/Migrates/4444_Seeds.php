<?php
use MediaCatalog\Models\Post;
use MediaCatalog\Models\Tag;

$posts = [
    Post::create([
        'title' => 'Wingsuit Proximity',
        'url' => 'https://www.youtube.com/embed/10byeZV5jcc',
        'type' => 'youtube',
        'preview' => 'https://i.ytimg.com/vi_webp/10byeZV5jcc/sddefault.webp',
        'advertising' => false
    ]),
    Post::create([
        'title' => 'SOUTHWEST. PROVEN HERE.',
        'url' => 'https://player.vimeo.com/video/158243063',
        'type' => 'vimeo',
        'preview' => 'https://i.vimeocdn.com/video/559713844.webp?mw=960&mh=540',
        'advertising' => true
    ]),
    Post::create([
        'title' => 'Carved in Mayhem',
        'url' => 'https://player.vimeo.com/video/159048928',
        'type' => 'vimeo',
        'preview' => 'https://i.vimeocdn.com/video/560697717.webp?mw=960&mh=540',
        'advertising' => false
    ])
];
$tags = [
    Tag::create(['name' => 'Wingsuit']),
    Tag::create(['name' => 'Southwest']),
    Tag::create(['name' => 'Traning']),
    Tag::create(['name' => 'BaseJamp']),
    Tag::create(['name' => 'Mountains']),
    Tag::create(['name' => 'Motivation']),
    Tag::create(['name' => 'Sky'])
];

$posts[0]->tags()->save($tags[0]);
$posts[0]->tags()->save($tags[3]);
$posts[0]->tags()->save($tags[4]);
$posts[0]->tags()->save($tags[5]);
$posts[0]->tags()->save($tags[6]);
$posts[1]->tags()->save($tags[1]);
$posts[1]->tags()->save($tags[4]);
$posts[1]->tags()->save($tags[6]);
$posts[2]->tags()->save($tags[2]);
$posts[2]->tags()->save($tags[5]);