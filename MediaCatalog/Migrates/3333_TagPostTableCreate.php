<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

//Pivot table
Capsule::schema()->create('post_tag', function(Blueprint $table){
    $table->unsignedInteger('post_id');
    $table->unsignedInteger('tag_id');

    $table->foreign('post_id')->references('id')->on('posts');
    $table->foreign('tag_id')->references('id')->on('tags');
});