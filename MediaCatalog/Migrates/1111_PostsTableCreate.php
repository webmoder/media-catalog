<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

Capsule::schema()->create('posts', function(Blueprint $table){
    $table->increments('id');
    $table->timestamps(); //created_at and updated_at columns
    $table->string('title');
    $table->string('url');
    $table->string('type'); //local, remote, youtube, vimeo etc.
    $table->string('preview');
    $table->boolean('advertising');
});

