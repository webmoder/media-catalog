<?php
/**
 * Created by PhpStorm.
 * User: web-m
 * Date: 22.03.2016
 * Time: 16:12
 */
use MediaCatalog\Http\Route;

Route::get('/', 'MediaCatalog\Controllers\PostsController@index');
Route::get('/posts/(\d+?)', 'MediaCatalog\Controllers\PostsController@show');
Route::get('/tags/(\d+?)', 'MediaCatalog\Controllers\TagsController@show');