<?php
/**
 * Created by PhpStorm.
 * User: web-m
 * Date: 22.03.2016
 * Time: 0:23
 */

namespace MediaCatalog\Models;

/**
 * Class Post
 * @package MediaCatalog\Models
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|Tag[] $tags
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $title
 * @property string $url
 * @property string $type
 * @property string $preview
 * @property boolean $advertising
 */
class Post extends Model
{
    protected $guarded = ['id'];

    public function tags(){
        return $this->belongsToMany(Tag::class);
    }
}