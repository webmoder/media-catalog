<?php
/**
 * Created by PhpStorm.
 * User: web-m
 * Date: 22.03.2016
 * Time: 0:24
 */

namespace MediaCatalog\Models;

/**
 * Class Tag
 * @package MediaCatalog\Models
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|Post[] $posts
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $name
 */
class Tag extends Model
{
    protected $guarded = ['id'];
    public function posts(){
        return $this->belongsToMany(Post::class);
    }
}