<?php
/**
 * Created by PhpStorm.
 * User: web-m
 * Date: 22.03.2016
 * Time: 15:44
 */

namespace MediaCatalog\Http;


class Route
{
    static protected $routes = [
        'get' => []
        /*
         * post, put, delete, etc.
         */
    ];
    static protected $controllers = [];

    static public function get($route, $controllerActionable){

        //if $controllerActionable is Closure
        if($controllerActionable instanceof Closure){
            self::$routes['get'][$route] = $controllerActionable;
        } //if $controllerActionable is callable
        elseif(is_callable($controllerActionable)){
            self::$routes['get'][$route] = function(...$args) use($controllerActionable){
                return call_user_func_array($controllerActionable, $args);
            };
        } //if $controllerActionable is string "ControllerName@action"
        elseif(is_string($controllerActionable) && strpos($controllerActionable, '@')){
            list($controllerName, $action) = explode('@', $controllerActionable);
            if(key_exists($controllerName, self::$controllers)){
                $controller = self::$controllers[$controllerName];
            }else{
                $controller = new $controllerName;
                self::$controllers[$controllerName] = $controller;
            }

            self::$routes['get'][$route] = function(...$args) use($controller, $action){
                return call_user_func_array([$controller, $action], $args);
            };
        }
    }

    static public function execute($route, $method){

        foreach(self::$routes[$method] as $routeRegex => $action){
            if(preg_match('#^'.str_replace('/', '\/', $routeRegex).'$#i', $route, $matches)){
                array_shift($matches);
                return call_user_func_array($action, $matches);
            }
        }
    }
}