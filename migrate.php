<?php
/**
 * Created by PhpStorm.
 * User: web-m
 * Date: 23.03.2016
 * Time: 14:50
 */
require_once __DIR__ . '/bootstrap.php';

require_once __DIR__ . '/MediaCatalog/Migrates/1111_PostsTableCreate.php';
require_once __DIR__ . '/MediaCatalog/Migrates/2222_TagsTableCreate.php';
require_once __DIR__ . '/MediaCatalog/Migrates/3333_TagPostTableCreate.php';
require_once __DIR__ . '/MediaCatalog/Migrates/4444_Seeds.php';